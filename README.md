# Connecter la base OCS au serveur GLPI

Surl'onglet tester, si on a ça :

![](images/glpi_plugin_failure.png)

aller dans les options du serveur OCS :

![](images/glpi_plugin_solution.png)

# Importer les logiciels dans l'inventaire

Changer les données à importer :

![](images/glpi_plugin_software_import.png)

Changer les options d'importation :

![](images/glpi_plugin_software_dictionnary.png
)

# Garder les fichiers d'inventaire

![](images/ocs_keep_inventory_files.png)
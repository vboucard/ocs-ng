yum install -y \
https://repo.ius.io/ius-release-el7.rpm \
https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
yum install -y https://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum install -y https://rpm.ocsinventory-ng.org/ocsinventory-release-latest.el7.ocs.noarch.rpm

yum update -y
yum install -y httpd24u httpd24u-mod_ssl


yum install -y yum-utils
yum-config-manager --disable 'remi-php*'
yum-config-manager --enable remi-php82
yum-config-manager --enable remi

yum install -y ocsinventory-release-latest.el7.ocs.noarch.rpm

yum install -y php php-cli php-pdo php-mysqlnd
yum install -y unzip curl wget lsof bind-utils bash-completion mlocate bzip2 tcpdump jq
yum install -y policycoreutils-python tree
yum install -y php-gd php-mbstring php-intl php-xml php-json php-ldap
yum install -y php-pecl-xmlrpc php-imap php-curl php-pecl-zip php-opcache

systemctl enable httpd --now
firewall-cmd --add-service=http --add-service=https --perm
firewall-cmd --reload

yum install -y mariadb104-server

systemctl enable mariadb --now
mariadb -uroot < /vagrant/mariadb_secure_installation.sql

yum install -y ocsinventory

systemctl restart httpd